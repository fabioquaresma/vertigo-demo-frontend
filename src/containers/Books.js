/* eslint-disable jsx-a11y/alt-text */
import React from "react";

function Book({ book, handleReserveBooks, selecteds }) {
  return (
    <div>
      <article className="media">
        <div className="media-left">
          <figure className="image is-64x64">
            <img src="https://bulma.io/images/placeholders/128x128.png" />
          </figure>
        </div>
        <div className="media-content">
          <div className="content">
            <strong>{book.tittle}</strong> <small>{book.author}</small>{" "}
            <small>{book.category}</small>
            <br />
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
            efficitur sit amet massa fringilla egestas. Nullam condimentum
            luctus turpis.
            <br />
            <form onSubmit={handleReserveBooks}>
              <input type="hidden" name="book" value={book._id} />
              <div className="buttons">
                <label className="checkbox">
                  <p>
                    <input
                      type="checkbox"
                      name={`item${book._id}`}
                      id={`item${book._id}`}
                      value={book._id}
                      onChange={handleReserveBooks}
                    />

                    <small>
                      &nbsp;<strong>Selecionar</strong>
                    </small>
                  </p>
                </label>
              </div>
            </form>
          </div>
        </div>
      </article>
      <hr />
    </div>
  );
}

export default Book;

/* eslint-disable jsx-a11y/img-redundant-alt */
import React from "react";

function Login({
  handleChange,
  handleBack,
  handleRegister,
  msgValidate,
  login
}) {
  return (
    <div>
      <div className="columns is-mobile">
        <div className="column is-half is-offset-one-quarter">
          {msgValidate && (
            <div className="notification is-warning">{msgValidate}</div>
          )}
          <hr />
          <h2 className="subtitle">
            Identifique-se para concluir sua reserva!
          </h2>
          <div className="field">
            <p className="control has-icons-left has-icons-right">
              <input
                id="email"
                name="email"
                className="input"
                type="email"
                placeholder="Email"
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-envelope" />
              </span>
              <span className="icon is-small is-right">
                <i className="fas fa-check" />
              </span>
            </p>
          </div>
          <div className="field">
            <p className="control has-icons-left">
              <input
                className="input"
                name="password"
                type="password"
                placeholder="Senha"
                onChange={handleChange}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-lock" />
              </span>
            </p>
          </div>
          <div className="field">
            <div className="field is-grouped">
              <p className="control">
                <button className="button is-success" onClick={login}>
                  Acessar
                </button>
              </p>
              <p className="control">
                <a
                  className="button is-light"
                  href="true"
                  onClick={handleRegister}
                >
                  Cadastre-se
                </a>
              </p>
              <p className="control">
                <a className="button is-text" href="true" onClick={handleBack}>
                  Voltar
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;

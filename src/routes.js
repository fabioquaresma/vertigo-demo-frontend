import Login from "./components/Login";
import Books from "./components/Books";
import Success from "./components/Success";
import Reserve from "./components/Reserve";
import Register from "./components/Register";

const routes = [
  {
    path: "/",
    component: Books
  },
  {
    path: "/auth",
    component: Login
  },
  {
    path: "/success",
    component: Success
  },
  {
    path: "/reserve",
    component: Reserve
  },
  {
    path: "/register",
    component: Register
  }
];

export default routes;

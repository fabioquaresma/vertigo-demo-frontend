export const BOOKS_FAILURE = "BOOKS_FAILURE";
export const LOADED_BOOKS = "LOADED_BOOKS";
export const FETCH_BOOKS = "FETCH_BOOKS";
export const FIND_BOOKS_CATEGORY = "FIND_BOOKS_CATEGORY";
export const SELECT_BOOKS = "SELECT_BOOKS";

export function booksFailure(error) {
  return { type: BOOKS_FAILURE, error };
}

export function loadedBooks(books) {
  return { type: LOADED_BOOKS, books };
}

export function fetchBooks() {
  return { type: FETCH_BOOKS };
}

export function findBooksByCategory(category) {
  return { type: FIND_BOOKS_CATEGORY, category };
}

export function selectBooks(book) {
  return { type: SELECT_BOOKS, book };
}

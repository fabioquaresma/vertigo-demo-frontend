export const LOGIN_FAILURE = "LOGIN_FAILURE";
export const LOGIN_START = "LOGIN_START";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const USER_REGISTER = "USER_REGISTER";

export function login(user) {
  return { type: LOGIN_START, user };
}

export function loginFailure(error) {
  return { type: LOGIN_FAILURE, error };
}

export function loadedUser(user) {
  return { type: LOGIN_SUCCESS, user };
}

export function register(user) {
  return { type: USER_REGISTER, user };
}

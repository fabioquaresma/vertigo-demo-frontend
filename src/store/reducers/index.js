import { combineReducers } from "redux";
import books, { BOOKS_DEFAULT_STATE } from "./books";
import users, { USERS_DEFAULT_STATE } from "./users";

const bookApp = combineReducers({
  books,
  users
});

export const DEFAULT_STATE = {
  books: BOOKS_DEFAULT_STATE,
  users: USERS_DEFAULT_STATE
};

export default bookApp;

import {
  BOOKS_FAILURE,
  LOADED_BOOKS,
  FETCH_BOOKS,
  FIND_BOOKS_CATEGORY,
  SELECT_BOOKS
} from "../actions/books";

export const BOOKS_DEFAULT_STATE = {
  loading: false,
  saving: false,
  error: "",
  items: [],
  selecteds: []
};

export default function books(state = BOOKS_DEFAULT_STATE, action) {
  switch (action.type) {
    case LOADED_BOOKS:
      return { ...state, items: action.books, loading: false };

    case FIND_BOOKS_CATEGORY:
      return { ...state, loading: false };

    case FETCH_BOOKS: {
      return { ...state, loading: true };
    }
    case SELECT_BOOKS:
      return { ...state, selecteds: action.book, loading: false };

    case BOOKS_FAILURE:
      return { ...state, loading: false, saving: false, error: action.error };

    default:
      return state;
  }
}

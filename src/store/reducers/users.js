import {
  LOGIN_START,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  USER_REGISTER
} from "../actions/users";

export const USERS_DEFAULT_STATE = {
  loading: false,
  saving: false,
  error: "",
  user: null
};

export default function users(state = USERS_DEFAULT_STATE, action) {
  switch (action.type) {
    case LOGIN_START:
      return { ...state, loading: false };

    case LOGIN_SUCCESS:
      return { ...state, user: action.user, loading: false };

    case USER_REGISTER:
      return { ...state, loading: false };

    case LOGIN_FAILURE:
      return { ...state, loading: false, saving: false, error: action.error };

    default:
      return state;
  }
}

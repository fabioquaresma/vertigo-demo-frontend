import { call, put, takeLatest } from "redux-saga/effects";
import {
  FETCH_BOOKS,
  FIND_BOOKS_CATEGORY,
  loadedBooks,
  booksFailure
} from "./actions/books";

import {
  LOGIN_START,
  USER_REGISTER,
  loadedUser,
  loginFailure
} from "./actions/users";

function* loginUser(action) {
  try {
    const options = {
      method: "POST",
      body: JSON.stringify(action.user),
      headers: new Headers({
        "Content-Type": "application/json"
      })
    };

    const res = yield call(
      fetch,
      "http://localhost:4000/api/v1/user/login",
      options
    );

    const user = yield res.json();

    if (res.status === 500) {
      yield put(loginFailure(user.msg));

      return;
    }

    yield put(loadedUser(user));
  } catch (e) {
    yield put(loginFailure(e.message));
  }
}

function* userRegister(action) {
  try {
    const options = {
      method: "POST",
      body: JSON.stringify(action.user),
      headers: new Headers({
        "Content-Type": "application/json"
      })
    };

    yield call(fetch, "http://localhost:4000/api/v1/user", options);
  } catch (e) {
    yield put(loginFailure(e.message));
  }
}

function* getAllBooks() {
  try {
    const res = yield call(fetch, "http://localhost:4000/api/v1/books");
    const books = yield res.json();
    yield put(loadedBooks(books));
  } catch (e) {
    yield put(booksFailure(e.message));
  }
}

function* findBooksByCategory(action) {
  try {
    const res = yield call(
      fetch,
      `http://localhost:4000/api/v1/books/${action.category}`
    );
    const books = yield res.json();
    yield put(loadedBooks(books));
  } catch (e) {
    yield put(booksFailure(e.message));
  }
}

function* rootSaga() {
  yield takeLatest(FETCH_BOOKS, getAllBooks);
  yield takeLatest(FIND_BOOKS_CATEGORY, findBooksByCategory);
  yield takeLatest(LOGIN_START, loginUser);
  yield takeLatest(USER_REGISTER, userRegister);
}

export default rootSaga;

/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Route, BrowserRouter } from "react-router-dom";

import routes from "./routes";

import "bulma/css/bulma.css";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="container">
        <br />
        <nav className="level">
          <div className="container has-text-centered">
            <h1 className="title">Vertigo Biblioteca</h1>
            <h2 className="subtitle">Reserva de Livros</h2>
          </div>
        </nav>
        <BrowserRouter>
          {routes.map(route => (
            <Route
              exact
              key={route.path}
              path={route.path}
              component={route.component}
            />
          ))}
        </BrowserRouter>
      </div>
    );
  }
}

export default App;

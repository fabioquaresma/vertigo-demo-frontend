import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { login } from "../store/actions/users";

class Reserve extends Component {
  constructor() {
    super();
    this.state = { scheduler: {}, msgValidate: "" };
  }

  handleChange = e => {
    e.preventDefault();

    const value = e.target.value;
    const name = e.target.name;

    this.setState(prevState => {
      return {
        scheduler: {
          ...prevState.scheduler,
          [name]: value
        }
      };
    });
  };

  confirmReserve = e => {
    e.preventDefault();

    const { scheduler } = this.state;

    if (!scheduler.hasOwnProperty("start") || scheduler.start.length === 0) {
      this.setState({
        msgValidate: "Informe a data de reserva"
      });

      return;
    }

    if (!scheduler.hasOwnProperty("end") || scheduler.end.length === 0) {
      this.setState({
        msgValidate: "Informe a data de devolução"
      });

      return;
    }

    this.props.history.push("/success");
  };

  render() {
    if (!this.props.loggedUser) return <Redirect to="/" />;

    return (
      <div className="column is-offset-two-quarter">
        {this.state.msgValidate && (
          <div className="notification is-warning">
            {this.state.msgValidate}
          </div>
        )}
        <hr />
        <h2 className="subtitle">
          Livros selecionados por <strong>{this.props.loggedUser.name}</strong>:
        </h2>
        <div className="columns is-mobile">
          <div className="column">
            <ol type="1">
              {this.props.selecteds.map((item, i) => (
                <li key={i}>{item.tittle}</li>
              ))}
            </ol>
          </div>
          <div className="column">
            <div className="field">
              <label className="label">Data de reserva</label>
              <div className="control">
                <input
                  name="start"
                  className="input"
                  type="date"
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>
            <div className="field">
              <label className="label">Data de devolução</label>
              <div className="control">
                <input
                  name="end"
                  className="input"
                  type="date"
                  onChange={e => this.handleChange(e)}
                />
              </div>
            </div>
            <div className="field is-grouped">
              <div className="control">
                <button
                  className="button is-link"
                  onClick={e => this.confirmReserve(e)}
                >
                  Confirma
                </button>
              </div>
              <div className="control">
                <button
                  className="button is-text"
                  onClick={() => this.props.history.push("/")}
                >
                  Cancela
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selecteds: state.books.selecteds,
    loggedUser: state.users.user
  };
};

const mapDispatchToProps = {
  login
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reserve);

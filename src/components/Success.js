import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { login } from "../store/actions/users";

class Reserve extends Component {
  render() {
    if (!this.props.loggedUser) return <Redirect to="/" />;

    return (
      <div className="column is-half is-offset-one-quarter">
        <hr />
        <article className="message is-primary">
          <div className="message-header">
            <p>Sucesso!</p>
            <button
              className="button"
              onClick={() => this.props.history.push("/")}
            >
              Voltar
            </button>
          </div>
          <div className="message-body">
            Parabéns {this.props.loggedUser.name}, reserva efeutada com sucesso!{" "}
            <br />
            Anote seu protcolo: <strong>{Math.random()}</strong>
          </div>
        </article>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selecteds: state.books.selecteds,
    loggedUser: state.users.user
  };
};

const mapDispatchToProps = {
  login
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Reserve);

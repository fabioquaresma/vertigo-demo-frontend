import React, { Component } from "react";
import { connect } from "react-redux";

import { register } from "../store/actions/users";

class Register extends Component {
  constructor() {
    super();
    this.state = { user: {}, msgValidate: "" };
  }

  handleChange = e => {
    e.preventDefault();

    const value = e.target.value;
    const name = e.target.name;

    this.setState(prevState => {
      return {
        user: {
          ...prevState.user,
          [name]: value
        }
      };
    });
  };

  register = e => {
    const { user } = this.state;

    if (!user.hasOwnProperty("name") || user.name.length === 0) {
      this.setState({
        msgValidate: "Informe seu nome"
      });

      return;
    }

    if (!user.hasOwnProperty("email") || user.email.length === 0) {
      this.setState({
        msgValidate: "Informe seu email"
      });

      return;
    }

    if (!user.hasOwnProperty("password") || user.password.length === 0) {
      this.setState({
        msgValidate: "Informe sua senha"
      });

      return;
    }

    this.props.register(this.state.user);
    this.props.history.push("/auth");
  };

  render() {
    return (
      <div>
        <div className="columns is-mobile">
          <div className="column is-half is-offset-one-quarter">
            {this.state.msgValidate && (
              <div className="notification is-warning">
                {this.state.msgValidate}
              </div>
            )}
            <hr />
            <h2 className="subtitle">Registre-se para efetivar sua reserva!</h2>
            <div className="field">
              <p className="control has-icons-left has-icons-right">
                <input
                  id="name"
                  name="name"
                  className="input"
                  type="name"
                  placeholder="Nome"
                  onChange={e => this.handleChange(e)}
                />
                <span className="icon is-small is-left">
                  <i className="fas fa-envelope" />
                </span>
                <span className="icon is-small is-right">
                  <i className="fas fa-check" />
                </span>
              </p>
            </div>
            <div className="field">
              <p className="control has-icons-left has-icons-right">
                <input
                  id="email"
                  name="email"
                  className="input"
                  type="email"
                  placeholder="Email"
                  onChange={e => this.handleChange(e)}
                />
                <span className="icon is-small is-left">
                  <i className="fas fa-envelope" />
                </span>
                <span className="icon is-small is-right">
                  <i className="fas fa-check" />
                </span>
              </p>
            </div>
            <div className="field">
              <p className="control has-icons-left">
                <input
                  className="input"
                  name="password"
                  type="password"
                  placeholder="Senha"
                  onChange={e => this.handleChange(e)}
                />
                <span className="icon is-small is-left">
                  <i className="fas fa-lock" />
                </span>
              </p>
            </div>
            <div className="field">
              <div className="field is-grouped">
                <p className="control">
                  <button
                    className="button is-success"
                    onClick={e => this.register(e)}
                  >
                    Registrar
                  </button>
                </p>
                <p className="control">
                  <a
                    className="button is-text"
                    onClick={() => this.props.history.push("/auth")}
                    href
                  >
                    Voltar
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = {
  register
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register);

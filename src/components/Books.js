import React, { Component } from "react";
import { connect } from "react-redux";

import {
  fetchBooks,
  findBooksByCategory,
  selectBooks
} from "../store/actions/books";

import Book from "../containers/Books";

class Books extends Component {
  constructor() {
    super();
    this.state = {
      selecteds: [],
      checkeds: new Map(),
      msgValidate: ""
    };
  }

  componentDidMount() {
    this.props.fetchBooks();
    this.props.selectBooks([]);
  }

  handleFilterBooks = e => {
    e.preventDefault();

    this.props.findBooksByCategory(e.target.value);
  };

  handleReserveBooks = e => {
    const item = e.target.value;
    const isChecked = e.target.checked;
    this.setState(prevState => ({
      checkeds: prevState.checkeds.set(item, isChecked)
    }));
  };

  confirmReserve = async e => {
    const itemsSelecteds = [...this.state.checkeds]
      .filter(item => item[1])
      .map(item => item[0]);

    const selecteds = this.props.books.filter(book =>
      itemsSelecteds.includes(book._id)
    );

    await this.props.selectBooks(selecteds);

    if (this.props.selecteds.length === 0) {
      this.setState({
        msgValidate: `Selecione pelo menos 1 para reserva`
      });

      setTimeout(() => {
        this.setState({
          msgValidate: ""
        });
      }, 3000);

      return;
    }

    this.props.history.push("/auth");
  };

  render() {
    return (
      <div>
        {this.state.msgValidate && (
          <div className="notification is-warning">
            {this.state.msgValidate}
          </div>
        )}

        <div className="field is-grouped is-grouped-left">
          <span className="control">
            <div className="select">
              <select
                className="is-focused"
                onChange={e => this.handleFilterBooks(e)}
              >
                <option value="">Todas as categorias</option>
                <option value="LITERATURA">Literatura</option>
                <option value="COMPUTACAO">Computação</option>
                <option value="FICCAO">Ficção</option>
              </select>
            </div>
          </span>
          <span className="control">
            <button
              className="button is-success"
              onClick={e => this.confirmReserve()}
            >
              Reservar livro(s) selecionado(s)
            </button>
          </span>
        </div>
        <hr />
        {this.props.books.map(book => (
          <Book
            key={book._id}
            book={book}
            handleReserveBooks={book => this.handleReserveBooks(book)}
          />
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    books: state.books.items,
    selecteds: state.books.selecteds,
    isLoading: state.books.loading,
    isSaving: state.books.saving,
    error: state.books.error
  };
};

const mapDispatchToProps = {
  fetchBooks,
  findBooksByCategory,
  selectBooks
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Books);

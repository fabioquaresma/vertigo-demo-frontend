import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import { login } from "../store/actions/users";

import LoginForm from "../containers/Login";

class Login extends Component {
  constructor() {
    super();
    this.state = { user: {}, msgValidate: "" };
  }

  login = e => {
    e.preventDefault();

    const { user } = this.state;

    if (!user.hasOwnProperty("email") || user.email.length === 0) {
      this.setState({
        msgValidate: "Informe seu email"
      });

      return;
    }

    if (!user.hasOwnProperty("password") || user.password.length === 0) {
      this.setState({
        msgValidate: "Informe sua senha"
      });

      return;
    }

    this.props.login(this.state.user);
  };

  handleBack = e => {
    e.preventDefault();
    this.props.history.push("/");
  };

  handleRegister = e => {
    e.preventDefault();
    this.props.history.push("/register");
  };

  handleChange = e => {
    e.preventDefault();

    const value = e.target.value;
    const name = e.target.name;

    this.setState(prevState => {
      return {
        user: {
          ...prevState.user,
          [name]: value
        }
      };
    });
  };

  componentDidUpdate(prevProps) {
    if (this.props.loggedUser !== prevProps.loggedUser) {
      this.props.history.push("/reserve");
    }
  }

  render() {
    if (this.props.loggedUser) return <Redirect to="/reserve" />;
    if (this.props.selecteds.length === 0) return <Redirect to="/" />;

    return (
      <div>
        <LoginForm
          handleChange={this.handleChange}
          handleBack={this.handleBack}
          handleRegister={this.handleRegister}
          login={this.login}
          msgValidate={this.state.msgValidate}
          history={this.props.history}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selecteds: state.books.selecteds,
    loggedUser: state.users.user
  };
};

const mapDispatchToProps = {
  login
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
